/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.bluetooth.leremote;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

/**
 * This class includes a small subset of standard GATT attributes for demonstration purposes.
 */
public class SampleGattAttributes {
    public HashMap<String, String> attributes = new HashMap();
    
    
	public SampleGattAttributes(Context mainActivity,String deviceID) {
		super();
		Log.d("DEVICE_CONFIG", "initConfig:"+deviceID);
		InputStream is;
		try {
			is = mainActivity.getAssets().open("sensorConfig.json");
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			String bufferString = new String(buffer);
			JSONObject objeto=new JSONObject(bufferString);
			JSONObject atributos=objeto.getJSONObject(deviceID);
			if (atributos==null){
				atributos= objeto.getJSONObject((String) objeto.keys().next());
			}
			Iterator<String> nameItr = atributos.keys();
			while(nameItr.hasNext()) {
				String name=nameItr.next();
				attributes.put(name, atributos.getString(name));
				Log.d("DEVICE_CONFIG", "ADD:"+name);
			}   
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}


	public String lookup(String uuid, String unknownServiceString) {
		String dd=attributes.get(uuid);
		if (dd==null)
			dd=unknownServiceString;
		return dd;
	}

	public String getIt(String value){
		String salida="";
		for (String ss :attributes.keySet()){
			if (attributes.get(ss).equals(value)){
				return ss;
			}
		}
		return salida;
	}
    
    
}
